import datetime

from django.utils import timezone
from django.test import TestCase
from django.urls import reverse

from .models import User, Project
from .serializers import ProjectWithUsersSerializer

def create_user(first_name, last_name, email):
    return User.objects.create(first_name = first_name, last_name = last_name, email = email)

def create_project(key, name):
    return Project.objects.create(key = key, name = name)

def add_user_to_project(user, project):
    project.users.add(user)

class UserModelTests(TestCase):

    def test_is_new_user_with_old_user(self):
        created_at = timezone.now() - datetime.timedelta(days=30)
        u = User(first_name="George", last_name="Lucas", email="george.lucas@lucasarts.com", created_at=created_at)
        self.assertIs(u.is_new_user(), False)

    def test_is_new_user_with_new_user(self):
        u = User(first_name="Jeffrey Jacob", last_name="Abrams", email="jj.abrams@lucasarts.com", created_at=timezone.now())
        self.assertIs(u.is_new_user(), True)

    def test_user_with_no_projects(self):
        u = create_user("George", "Lucas", "george.lucas@lucasarts.com")
        projects = Project.objects.filter(users__in=[u])
        self.assertEquals(list(u.projects()), list(projects))

    def test_user_with_projects(self):
        u = create_user("George", "Lucas", "george.lucas@lucasarts.com")
        p = create_project("chapter_1", "A New Hope")
        add_user_to_project(u, p)
        projects = Project.objects.filter(users__in=[u])
        self.assertEquals(list(u.projects()), list(projects))

class ProjectListViewTests(TestCase):

    def test_no_projects(self):
        response = self.client.get(reverse('projects:project_list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_with_project(self):
        u = create_user("George", "Lucas", "george.lucas@lucasarts.com")
        p = create_project("chapter_1", "A New Hope")
        add_user_to_project(u, p)
        projects = Project.objects.order_by('-created_at')
        data = ProjectWithUsersSerializer(projects, many=True).data
        response = self.client.get(reverse('projects:project_list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data, data)
