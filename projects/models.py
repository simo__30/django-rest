import datetime

from django.contrib import admin
from django.core import validators
from django.db import models
from django.utils import timezone


class User(models.Model):
    first_name = models.CharField(max_length=512)
    last_name = models.CharField(max_length=512)
    email = models.CharField(max_length=512, unique=True, validators=(
        validators.EmailValidator(
            message='Invalid email format'
        ),
    ), )
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

    @admin.display(
        boolean=True,
        ordering='is_new_user',
        description='Created recently?',
    )
    def is_new_user(self):
        return timezone.now() - datetime.timedelta(days=30) <= self.created_at

    def projects(self):
        return self.project_set.all()

class Project(models.Model):
    key = models.CharField(max_length=32, unique=True, validators=(
        validators.RegexValidator(
            regex=r"^[a-z0-9_]*$", message='Allowed characters: "a-z", "0-9", "_"'
        ),
    ), )
    name = models.CharField(max_length=512, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    users = models.ManyToManyField(User)

    def __str__(self):
        return self.name
