from rest_framework import serializers

from .models import Project, User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name')

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'key', 'name')

class ProjectWithUsersSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True)

    class Meta:
        model = Project
        fields = ('id', 'key', 'name', 'users')

class UserWithProjectsSerializer(serializers.ModelSerializer):
    projects = ProjectSerializer(many=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'is_new_user', 'projects')
