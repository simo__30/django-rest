from django.urls import path

from . import views

app_name = 'projects'
urlpatterns = [
    path('api/project/', views.ProjectListCreate.as_view(), name='project_list'),
    path('api/user/<int:pk>', views.UserDetail.as_view()),
]
