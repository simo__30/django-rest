from django.contrib import admin

from .models import Project, User

class ProjectAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Info', {'fields': ['key', 'name']}),
        ('Users', {'fields': ['users']})
    ]
    list_display = ('key', 'name')
    search_fields = ['name']

class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'is_new_user')

admin.site.register(Project, ProjectAdmin)
admin.site.register(User, UserAdmin)
